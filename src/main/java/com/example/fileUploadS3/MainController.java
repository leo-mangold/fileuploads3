package com.example.fileUploadS3;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @GetMapping("")
    public String showHomePage(){
        return "upload";
    }

    @PostMapping("/upload")
    public String handleUploadForm(Model model , @RequestParam("file") MultipartFile multipart) {
        String fileName = multipart.getOriginalFilename();

        System.out.println("filename: " + fileName);

        String message = "";

        try {
            S3Util.uploadFile(fileName, multipart.getInputStream());
            message = "El archivo "+fileName+" se subió correctamente!";
        } catch (Exception ex) {
            message = "Error uploading file: " + ex.getMessage();
        }

        model.addAttribute("message", message);

        return "message";
    }

    @GetMapping("/list")
    public String listarImagenes(Model model) {
        List<S3Object> s3Images = S3Util.listFiles();

        model.addAttribute("images", s3Images);
        return "images";
    }

    @PostMapping("/delete/{name}")
    public String deleteImage (Model model, @PathVariable String name) {
        S3Util.deleteImage(name);
        return "redirect:/list";
    }


}
