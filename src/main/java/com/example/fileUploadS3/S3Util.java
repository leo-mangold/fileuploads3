package com.example.fileUploadS3;


import org.springframework.beans.factory.annotation.Value;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class S3Util {

    private static final String BUCKET = "xsidefileupload";

    @Value("${cloud.aws.credentials.accessKey:AKIAQAXNYT227WNON2LF}")
    private String s3key;

    @Value("${cloud.aws.credentials.secretKey:83vyg7Qyv95FPZ0qPT0r5p25Xl6qZRBSZkRU2LpM}")
    private String s3Password;

    @Value("${aws.s3.access.region:us-west-2}")
    private String region;

    @Value("${aws.s3.access.name:xsidefileupload}")
    private String bucket;
    public static void uploadFile(String fileName, InputStream inputStream)
            throws S3Exception, AwsServiceException, SdkClientException, IOException {


        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(
                "AKIAQAXNYT227WNON2LF",
                "83vyg7Qyv95FPZ0qPT0r5p25Xl6qZRBSZkRU2LpM");

        S3Client client = S3Client.builder()
                            .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                            .region(Region.US_WEST_2).build();

        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(BUCKET)
                .key(fileName)
                .contentType("image/png")
                .build();

        client.putObject(request,
                RequestBody.fromInputStream(inputStream, inputStream.available()));
    }

    public static List<S3Object> listFiles(){
        List<S3Object> imagenes = new ArrayList<>();
        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(
                "AKIAQAXNYT227WNON2LF",
                "83vyg7Qyv95FPZ0qPT0r5p25Xl6qZRBSZkRU2LpM");

        S3Client client = S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .region(Region.US_WEST_2).build();

        ListObjectsRequest request = ListObjectsRequest.builder().bucket(BUCKET).build();
        ListObjectsResponse response = client.listObjects(request);
        List<S3Object> objects = response.contents();

        ListIterator<S3Object> listIterator = objects.listIterator();

        while (listIterator.hasNext()) {
            S3Object object = listIterator.next();
            imagenes.add(object);
        }
        return imagenes;
    }

    public static void deleteImage(String nombre){

        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(
                "AKIAQAXNYT227WNON2LF",
                "83vyg7Qyv95FPZ0qPT0r5p25Xl6qZRBSZkRU2LpM");

        S3Client client = S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .region(Region.US_WEST_2).build();


        DeleteObjectRequest request = DeleteObjectRequest.builder()
                .bucket(BUCKET)
                .key(nombre)
                .build();

        client.deleteObject(request);
    }
}
